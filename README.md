# Dandelions

## Notes

## Credits

Flower definitions have been adapted from the flowers mod from minetest_game. The flowers mod was released under the MIT license by Ironzorg, VanessaE, and various Minetest developers and contributors.

## Media

Hugues Alexandre Ross (CC BY-SA 4.0):
 - flowers_dandelion_white.png
 - flowers_dandelion_yellow.png
